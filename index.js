document.getElementById('grade').addEventListener('submit', function(event) {
    event.preventDefault();
    const marks = document.getElementById('marks').value;
    let grade;

    if (marks >= 90) {
        grade = 'A+';
    } else if (marks >= 80) {
        grade = 'A';
    } else if (marks >= 70) {
        grade = 'B+';
    } else if (marks >= 60) {
        grade = 'B';
    } else if (marks >= 50) {
        grade = 'C+';
    } else if (marks >= 40) {
        grade = 'C';
    } else {
        grade = 'F';
    }

    const result = document.getElementById('result');
    result.innerHTML = `Your grade is: ${grade}`;
});
